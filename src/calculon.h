/**************************************************************************************************/
/*
 * File: wordrank.h
 * Author: Kevin Klug
 * Date: 29 September 2011
 *
 * Description: Functions for the implementation of ECE275 Assigment 2: Wordrank
 *
 */
/**************************************************************************************************/

#include "global.h"

/**************************************************************************************************/

#ifndef CALCULON_H
#define CALCULON_H

//Populates the stack
float populate_stack( char *inputFileName, Stack *stack);


#endif

/**************************************************************************************************/

