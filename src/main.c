/**************************************************************************************************/
/*
 * File: main.c
 * Author: Kevin Klug
 * Date: 14 October 2011
 *
 * Description: Template source file for main() function implementaiton for ECE 275 
 *              Assignment 3.
 *
 */
/**************************************************************************************************/

#include <stdio.h>
#include "stack.h"
#include "calculon.h"
#include "global.h"

#define MAXNUMBER 100

int main( int argc, char *argv[]){
    
    Stack stack;
    stack_init(&stack);
    
    //Check for correct usage of calculon
    if (argc != 2) {
        printf("\nUsage: calculon inputfile \n\n");
        return FALSE;
    }
    else {
        //Initalize a the stack
        stack_init(&stack);
        
        //populate_stack preforms all of the scanning and calculations on on the stack.
        //This function is located in calculon.h
        printf(" = %f\n", populate_stack(argv[1], &stack));
        return TRUE;
    }
}
