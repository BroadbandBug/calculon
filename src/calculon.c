/**************************************************************************************************/
/*
 * File: wordrank.h
 * Author: Kevin Klug
 * Date: 29 September 2011
 *
 * Description: Functions for the implementation of ECE275 Assigment 2: Wordrank
 *
 */
/**************************************************************************************************/

#include "global.h"
#include "stack.h"
#include "calculon.h"
#include <stdlib.h>
#include <stdio.h>

/**************************************************************************************************/
//Populates the stack and calculates the value resultant value of scanned in reverse polish notation
//inputfile.
/**************************************************************************************************/
float populate_stack( char *inputFileName, Stack *stack){
    
    char buff[BIGGESTNUMBER];
    
    float number = 0;
    
    float first_in = 0;
    float last_in = 0;
    
    float tmp = 0;
    
    FILE *inputFile;
    //Open File
    inputFile = fopen(inputFileName, "r");
    
    //Check if file exists
    if(inputFile == NULL){
        printf("Error: Cannot open file.\n");
        exit(FALSE);
    }
    
    while((fscanf(inputFile, "%s", buff) != 0) && !feof(inputFile)){
        
        if(sscanf(buff, "%f", &number) == 0){
            
            printf("%c", buff[0]);
            if(stack_size(stack) < 2){
                printf("\nError: Invalid input formatting. Calculon will try to continue...\n");
            }
            
            //This is done mostly because in reverse polish notation 4 6 - is equivalent to 4-6.
            //Without these temporary variables the order of calculation would be the opposite
            //of desired.
            last_in = stack_pop(stack);
            first_in = stack_pop(stack);
            
            switch (buff[0]) {
                case '+':
                    
                    tmp = first_in + last_in;
                    stack_push(stack, tmp);
                    break;
                    
                case '-':
                    tmp = first_in - last_in;
                    stack_push(stack, tmp);
                    break;
                    
                case '*':
                    tmp = first_in * last_in;
                    stack_push(stack, tmp);
                    break;
                    
                case '/':
                    
                    tmp = first_in / last_in;
                    stack_push(stack, tmp);
                    break;
                    
                default:
                    
                    printf("\nError: Invalid operator detected\n");
                    return FALSE;
                    break;
            }
        }else{
            if(stack_push(stack, number) == TRUE){
            printf("%f ", number);
            }else{
                printf("\n\nError in stack push\n");
            }
        }
        
    }
    
if(stack_size(stack) > 1){
        printf("\nError: Stack has not been simplified due to formatting error\n");
    exit(FALSE);
    }
    return stack_pop(stack);
}

/**************************************************************************************************/
