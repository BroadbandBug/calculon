/**************************************************************************************************/
/*
 * File: stack.h
 * Author: Kevin Klug
 * Date: 19 October 2011
 *
 * Description: This library implements a stack with push/pop through the previously made linked list
 *              library.
 *
 */
/**************************************************************************************************/

#include "global.h"
#include "listelmt.h"
#include "list.h"
/**************************************************************************************************/


#ifndef STACK_H
#define STACK_H

//
typedef List Stack;

void stack_init( Stack *stack );

void stack_destroy ( Stack *stack );

bool stack_push( Stack *stack, float data);

float stack_pop( Stack *stack);

int stack_size( Stack *stack );


#endif

/**************************************************************************************************/

