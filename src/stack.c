/**************************************************************************************************/
/*
 * File: stack.c
 * Author: Kevin Klug
 * Date: 19 October 2011
 *
 * Description: These functions implement a stack via a singly linked list
 *              
 */
/**************************************************************************************************/

#include <stdio.h>
#include "global.h"
#include "list.h"
#include "stack.h"


//It should be noted that these functions depend upon list.h because the stack that is implemented
//is in the form of singly linked list. For this reason it is important to include list.h and list.c
//in the ./src directory

void stack_init( Stack *stack ){
    list_init(stack);
}

void stack_destroy ( Stack *stack ){
    
}

bool stack_push( Stack *stack, float data){
    return list_ins_next(stack, NULL , data);
}

float stack_pop( Stack *stack){
    return list_rem_next(stack, NULL);
    
}

int stack_size( Stack *stack ){
    return list_size(stack);
}